/*
 *  COPYRIGHT (c) 2019.
 *  Cesar Fuguet-Tortolero
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.rtems.org/license/LICENSE.
 */
/*
 * Copyright (c) 2013-2014 embedded brains GmbH.  All rights reserved.
 *
 *  embedded brains GmbH
 *  Dornierstr. 4
 *  82178 Puchheim
 *  Germany
 *  <info@embedded-brains.de>
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rtems.org/license/LICENSE.
 */
#include <rtems/bspIo.h>
#include <rtems/console.h>
#include <rtems/sysinit.h>
#include <rtems/termiostypes.h>

#include <bsp/fdt.h>
#include <libfdt.h>
#include <string.h>

#include "bsp.h"
#include "bsp/irq.h"
#include "dev/serial/soclib_tty.h"


/*
 * Prototypes of functions
 */
static bool __console_first_open(
    struct rtems_termios_tty      *tty,
    rtems_termios_device_context  *context,
    struct termios                *term,
    rtems_libio_open_close_args_t *args);

void __console_last_close(
    struct rtems_termios_tty      *tty,
    rtems_termios_device_context  *context,
    rtems_libio_open_close_args_t *args);

static int __console_poll_read(rtems_termios_device_context *context);

static void __console_write(
    rtems_termios_device_context *context,
    const char *buf,
    size_t len);

static bool __console_set_attributes(
    rtems_termios_device_context *base,
    const struct termios         *t);

#ifdef USE_FDT
static int __console_get_fdt_node(const void *fdt);
#endif

static void __console_probe(void);

static int __getchar(void);

static void __putchar(char c);


/*
 * Definitions
 */
typedef struct
{
  rtems_termios_device_context ctx;
  rtems_termios_tty *tty;

  uintptr_t base;
  char console_path[32];
  int channel;
} console_context;

static console_context console_device_instance[CONSOLE_MAX_DEVICES];
static size_t __console_count;
static console_context *sysconsole;

static bool __console_first_open(
    struct rtems_termios_tty      *tty,
    rtems_termios_device_context  *context,
    struct termios                *term,
    rtems_libio_open_close_args_t *args)
{
  console_context *ctx;

  /* unused parameters */
  (void)tty;
  (void)term;
  (void)args;

  ctx = RTEMS_CONTAINER_OF(context, console_context, ctx);
  ctx->tty = tty;

  return true;
}

void __console_last_close(
    struct rtems_termios_tty      *tty,
    rtems_termios_device_context  *context,
    rtems_libio_open_close_args_t *args)
{
  /* unused parameters */
  (void)tty;
  (void)context;
  (void)args;

  /* do nothing */
}

static int __console_poll_read(rtems_termios_device_context *context)
{
  console_context *ctx;
  ctx = RTEMS_CONTAINER_OF(context, console_context, ctx);
  return soclib_tty_getc(ctx->base, ctx->channel);
}

static void __console_write(
    rtems_termios_device_context *context,
    const char *buf,
    size_t len)
{
  console_context *ctx;

  if (buf == NULL) return;

  ctx = RTEMS_CONTAINER_OF(context, console_context, ctx);
  while ((*buf) && len--) {
    soclib_tty_putc(ctx->base, ctx->channel, *buf);
    buf++;
  }
}

static bool __console_set_attributes(
    rtems_termios_device_context *base,
    const struct termios         *t)
{
  return true;
}

#ifdef USE_FDT
static int __console_get_fdt_node(const void *fdt)
{
  const char *stdout_path;
  int node;

  node = fdt_path_offset(fdt, "/chosen");

  stdout_path = fdt_getprop(fdt, node, "stdout-path", NULL);
  if (stdout_path == NULL) {
    stdout_path = "";
  }

  return fdt_path_offset(fdt, stdout_path);
}

#define CONSOLE_IS_COMPATIBLE(node, type) \
  (strncmp(node, type, sizeof(type) + 1) == 0)

static void __console_probe(void)
{
  const void *fdt;
  int node;
  int console_node;

  fdt = bsp_fdt_get();
  console_node = __console_get_fdt_node(fdt);
  sysconsole = NULL;
  __console_count = 0;

  do {
    const char *compat;
    int compat_len;

    node = fdt_next_node(fdt, -1, NULL);
    compat = fdt_getprop(fdt, node, "compatible", &compat_len);
    if (!compat) continue;

    if (CONSOLE_IS_COMPATIBLE(compat, "soclib_tty")) {
      __console_count++;
      console_context *context = &console_device_instance[__console_count];
      rtems_termios_device_context_initialize(&context->ctx, "soclib_tty");

      /* when the current node is the one selected with the /chosen node, set
       * the sysconsole pointer */
      if (node == console_node)
        sysconsole = context;

      if (__console_count >= CONSOLE_MAX_DEVICES) break;
    }
  } while (node >= 0);

  if (__console_count) {
    BSP_output_char = __putchar;
    BSP_poll_char = __getchar;
  }
}
#else
static void __console_probe(void)
{
  console_context *context;

  __console_count = 0;

  context = &console_device_instance[__console_count];
  context->base = 0xc0000000UL;
  context->channel = __console_count++;
  rtems_termios_device_context_initialize(&context->ctx, "tty0");

  sysconsole = &console_device_instance[0];

  BSP_output_char = __putchar;
  BSP_poll_char = __getchar;
}
#endif

static void __putchar(char c)
{
  rtems_termios_device_context *ctx;

  if (sysconsole == NULL)
    return;

  ctx = (rtems_termios_device_context*)sysconsole;
  __console_write(ctx, &c, 1);
}

static int __getchar(void)
{
  rtems_termios_device_context *ctx;

  if (sysconsole == NULL)
    return -1;

  ctx = (rtems_termios_device_context*)sysconsole;
  return __console_poll_read(ctx);
}

const rtems_termios_device_handler __console_handler_polled = {
  .first_open = __console_first_open,
  .last_close = __console_last_close,
  .poll_read = __console_poll_read,
  .write = __console_write,
  .set_attributes = __console_set_attributes,
  .mode = TERMIOS_POLLED
};

rtems_status_code console_initialize(
    rtems_device_major_number major,
    rtems_device_minor_number minor,
    void *arg)
{
  int i;

  /* unused parameters */
  (void)major;
  (void)minor;
  (void)arg;

  if (!__console_count)
    return RTEMS_UNSATISFIED;

  rtems_termios_initialize();

  for (i = 0; i < __console_count; ++i) {
    const char device_path[] = "/dev/ttyX";
    console_context *ctx = &console_device_instance[i];

    strcpy(ctx->console_path, device_path);
    ctx->console_path[sizeof(device_path) - 2] = (char) ('0' + i);
    rtems_termios_device_install(
        ctx->console_path,
        &__console_handler_polled,
        NULL,
        &ctx->ctx);

    if (ctx == sysconsole)
      link(ctx->console_path, CONSOLE_DEVICE_NAME);
  }

  return RTEMS_SUCCESSFUL;
}

static void __early_output_char_init(char c)
{
  __console_probe();
  __putchar(c);
}

BSP_output_char_function_type BSP_output_char = __early_output_char_init;
BSP_polling_getchar_function_type BSP_poll_char = NULL;

RTEMS_SYSINIT_ITEM(
    __console_probe,
    RTEMS_SYSINIT_BSP_START,
    RTEMS_SYSINIT_ORDER_LAST);
