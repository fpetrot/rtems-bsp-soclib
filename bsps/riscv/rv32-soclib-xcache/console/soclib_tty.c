/*
 *  COPYRIGHT (c) 2019
 *  Cesar Fuguet Tortolero
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.rtems.org/license/LICENSE.
 */
#include "dev/serial/soclib_tty.h"
#include <rtems/score/basedefs.h>

#define SOCLIB_TTY_WRITE     0
#define SOCLIB_TTY_STATUS    1
#define SOCLIB_TTY_READ      2
#define SOCLIB_TTY_CONFIG    3
#define SOCLIB_TTY_SPAN      4

#define __soclib_tty_get_channel_base(base, channel) \
  ((base) + (((channel)*SOCLIB_TTY_SPAN) << 2))


static inline uint32_t __soclib_tty_get_register(
    uintptr_t addr,
    int i)
{
  volatile uint32_t* reg = (volatile uint32_t*)addr + i;
  return *reg;
}


static inline void __soclib_tty_set_register(
    uintptr_t addr,
    int i,
    uint32_t val)
{
  volatile uint32_t* reg = (volatile uint32_t*)addr + i;
  RTEMS_COMPILER_MEMORY_BARRIER();
  *reg = (volatile uint32_t)val;
}


void soclib_tty_putc(
    uintptr_t base,
    int channel,
    char c)
{
  uintptr_t __base = __soclib_tty_get_channel_base(base, channel);
  __soclib_tty_set_register(__base, SOCLIB_TTY_WRITE, c);
}


int soclib_tty_getc(
    uintptr_t base,
    int channel)
{
  uintptr_t __base = __soclib_tty_get_channel_base(base, channel);

  if (!soclib_tty_has_char(__base, channel))
    return -1;

  return __soclib_tty_get_register(__base, SOCLIB_TTY_READ);
}


int soclib_tty_has_char(
    uintptr_t base,
    int channel)
{
  uintptr_t __base = __soclib_tty_get_channel_base(base, channel);
  return __soclib_tty_get_register(__base, SOCLIB_TTY_STATUS) ? 1 : 0;
}
