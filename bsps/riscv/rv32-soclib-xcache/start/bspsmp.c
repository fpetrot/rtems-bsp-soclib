/*
 *  COPYRIGHT (c) 2019.
 *  Cesar Fuguet-Tortolero
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.rtems.org/license/LICENSE.
 */
/*
 *  COPYRIGHT (c) 1989-2011.
 *  On-Line Applications Research Corporation (OAR).
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.rtems.org/license/LICENSE.
 */
#include <rtems/score/cpu.h>
#include <rtems/score/riscv-utility.h>
#include <rtems/score/smpimpl.h>
#include <bsp/bootcard.h>
#include "dev/irq/soclib_xicu.h"
#include "bsp/irq.h"
#include "bsp.h"

static rtems_isr bsp_inter_processor_interrupt( void *v )
{
  Per_CPU_Control *self_cpu = _Per_CPU_Get();
  uintptr_t xicu_base = bsp_get_per_cpu_xicu_address(self_cpu);

  (void)v;

  soclib_xicu_wti_recv(xicu_base, _Per_CPU_Get_index(self_cpu));
  _SMP_Inter_processor_interrupt_handler(self_cpu);
}


uint32_t _CPU_SMP_Initialize( void )
{
  Per_CPU_Control *self_cpu = _Per_CPU_Get();
  int self_cpu_index = _Per_CPU_Get_index(self_cpu);
  uintptr_t xicu_base = bsp_get_per_cpu_xicu_address(self_cpu);

  /* enable the WTI register (mailbox) for the current cpu */
  soclib_xicu_wti_enable(xicu_base, self_cpu_index, self_cpu_index);

  if ( rtems_configuration_get_maximum_processors() > 1 ) {
    rtems_interrupt_handler_install(
      INTERRUPT_VECTOR_IPI,
      "IPI",
      RTEMS_INTERRUPT_SHARED,
      bsp_inter_processor_interrupt,
      NULL
    );
  }

  /* Return the number of CPUs in the system.
   *
   * Current implementation is a semi-dynamic approach that uses the
   * rtems_configuration_get_maximum_processors macro, which returns the number
   * of cores as defined by the application.
   *
   * The risk here is that the given processors is bigger than the actual
   * ones...
   */
  /* FIXME: this could be parsed from the device tree... */
  return rtems_configuration_get_maximum_processors();
}


/* NOTE: Executed (only) by the master processor.
 */
bool _CPU_SMP_Start_processor( uint32_t cpu_index )
{
  Per_CPU_Control *cpu = _Per_CPU_Get_by_index(cpu_index);
  uintptr_t xicu_base = bsp_get_per_cpu_xicu_address(cpu);

  /* acknowledge any pending WTI interrupt */
  soclib_xicu_wti_recv(xicu_base, cpu_index);

  /* enable the WTI register (mailbox) for the target cpu */
  soclib_xicu_wti_enable(xicu_base, cpu_index, cpu_index);

  return true;
}


void _CPU_SMP_Finalize_initialization( uint32_t cpu_count )
{
}


/* NOTE: Executed by all secondary processors.
 */
void _CPU_SMP_Prepare_start_multitasking( void )
{
  /* Enable interrupts */
  //_CPU_ISR_Enable(RISCV_MSTATUS_MIE);
}


void _CPU_SMP_Send_interrupt( uint32_t cpu_index )
{
  Per_CPU_Control *target_cpu = _Per_CPU_Get_by_index(cpu_index);
  uintptr_t xicu_base = bsp_get_per_cpu_xicu_address(target_cpu);

  /* write value into the corresponding mailbox to trigger an interrupt */
  soclib_xicu_wti_send(xicu_base, cpu_index, 0xbabef00d);
}


void bsp_start_on_secondary_processor( struct Per_CPU_Control *cpu_self )
{
  uint32_t cpu_index_self = _Per_CPU_Get_index(cpu_self);

  cpu_self->cpu_per_cpu.plic_hart_regs =
    (void*)bsp_get_per_cpu_xicu_address(cpu_self);

  if ((cpu_index_self < rtems_configuration_get_maximum_processors())
      && _SMP_Should_start_processor(cpu_index_self))
  {
    set_csr(mie, MIP_MEIP);
    return _SMP_Start_multitasking_on_secondary_processor(cpu_self);
  }

  _CPU_Thread_Idle_body(0);
}
