/*
 *  COPYRIGHT (c) 2019.
 *  Cesar Fuguet-Tortolero
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.rtems.org/license/LICENSE.
 */
/*
 * Copyright (c) 2018 embedded brains GmbH
 *
 * Copyright (c) 2015 University of York.
 * Hesham Almatary <hesham@alumni.york.ac.uk>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <bsp/irq.h>
#include <bsp/fatal.h>
#include <bsp/irq-generic.h>
#include <bsp/fatal.h>
#include <sys/bitset.h>

#include <rtems/score/percpu.h>
#include <rtems/score/smpimpl.h>
#include <rtems/score/riscv-utility.h>

#include "dev/irq/soclib_xicu.h"

static uint32_t *const xicu_base = (uint32_t *const)0xc2000000UL;

void _RISCV_Interrupt_dispatch(uintptr_t mcause, Per_CPU_Control *cpu_self)
{
  int cpu_index = _Per_CPU_Get_index(cpu_self);

  /* mask the interrupt bit */
  mcause &= ~0x80000000UL;

  switch (mcause) {
  case RISCV_INTERRUPT_EXTERNAL_MACHINE:
  {
    uintptr_t icu_addr = bsp_get_per_cpu_xicu_address(cpu_self);

    if (soclib_xicu_wti_get_active(icu_addr, cpu_index)) {
      bsp_interrupt_handler_dispatch(INTERRUPT_VECTOR_IPI);
      break;
    }

    if (soclib_xicu_timer_get_active(icu_addr, cpu_index)) {
      bsp_interrupt_handler_dispatch(INTERRUPT_VECTOR_TIMER);
      break;
    }
  }

  default:
    bsp_fatal(RISCV_FATAL_UNEXPECTED_INTERRUPT_EXCEPTION);
  }

  return;
}

uintptr_t bsp_get_per_cpu_xicu_address(Per_CPU_Control *cpu_self)
{
  /* FIXME for now, only mono-cluster platforms are supported so only one XICU
   * is implemented*/
  (void)cpu_self;

  return (uintptr_t)xicu_base;
}


rtems_status_code bsp_interrupt_facility_initialize(void)
{
  Per_CPU_Control *cpu = _Per_CPU_Get_by_index(0);

  /* We don't use a PLIC-compliant interrupt controller but our
   * XICU does perfectly the work */
  cpu->cpu_per_cpu.plic_hart_regs = (void*)xicu_base;

  _CPU_ISR_Enable(RISCV_MSTATUS_MIE);
  set_csr(mie, MIP_MEIP);

  return RTEMS_SUCCESSFUL;
}

static void __bsp_interrupt_vector_enable(
    rtems_vector_number vector,
    int cpu_index)
{
  Per_CPU_Control *cpu_self = _Per_CPU_Get_by_index(cpu_index);
  uintptr_t xicu_addr = bsp_get_per_cpu_xicu_address(cpu_self);

  if (!bsp_interrupt_is_valid_vector(vector))
    return;

  switch(vector) {
  case INTERRUPT_VECTOR_TIMER:
    soclib_xicu_timer_enable(xicu_addr, cpu_index, cpu_index);
    break;
  case INTERRUPT_VECTOR_IPI:
    soclib_xicu_wti_enable(xicu_addr, cpu_index, cpu_index);
    break;
  }
}


static void __bsp_interrupt_vector_disable(
    rtems_vector_number vector,
    int cpu_index)
{
  Per_CPU_Control *cpu_self = _Per_CPU_Get_by_index(cpu_index);
  uintptr_t xicu_addr = bsp_get_per_cpu_xicu_address(cpu_self);

  if (!bsp_interrupt_is_valid_vector(vector))
    return;

  switch(vector) {
  case INTERRUPT_VECTOR_TIMER:
    soclib_xicu_timer_disable(xicu_addr, cpu_index, cpu_index);
    break;
  case INTERRUPT_VECTOR_IPI:
    soclib_xicu_wti_disable(xicu_addr, cpu_index, cpu_index);
    break;
  }
}


void bsp_interrupt_vector_enable(rtems_vector_number vector)
{
  __bsp_interrupt_vector_enable(vector, _Per_CPU_Get_index(_Per_CPU_Get()));
}


void bsp_interrupt_vector_disable(rtems_vector_number vector)
{
  __bsp_interrupt_vector_disable(vector, _Per_CPU_Get_index(_Per_CPU_Get()));
}


void bsp_interrupt_set_affinity(
  rtems_vector_number vector,
  const Processor_mask *affinity
)
{
  uint32_t cpu_count = rtems_scheduler_get_processor_maximum();
  uint32_t cpu_index;

  for (cpu_index = 0; cpu_index < cpu_count; ++cpu_index) {
    if (_Processor_mask_Is_set(affinity, cpu_index)) {
      __bsp_interrupt_vector_enable(vector, cpu_index);
    }
  }
}


void bsp_interrupt_get_affinity(
  rtems_vector_number vector,
  Processor_mask *affinity)
{
  uint32_t cpu_count = rtems_scheduler_get_processor_maximum();
  uint32_t cpu_index;
  uint32_t xicu_mask;
  uintptr_t xicu_addr = bsp_get_per_cpu_xicu_address(_Per_CPU_Get());

  /* reset result bitvector to 0 */
  _Processor_mask_Zero(affinity);

  switch(vector) {
  case INTERRUPT_VECTOR_TIMER:
    for (cpu_index = 0; cpu_index < cpu_count; ++cpu_index) {
      xicu_mask = soclib_xicu_timer_get_mask(xicu_addr, cpu_index);
      if ((xicu_mask >> cpu_index) & 0x1)
        _Processor_mask_Set(affinity, cpu_index);
    }
    break;
  case INTERRUPT_VECTOR_IPI:
    for (cpu_index = 0; cpu_index < cpu_count; ++cpu_index) {
      xicu_mask = soclib_xicu_wti_get_mask(xicu_addr, cpu_index);
      if ((xicu_mask >> cpu_index) & 0x1)
        _Processor_mask_Set(affinity, cpu_index);
    }
    break;
  }
}
