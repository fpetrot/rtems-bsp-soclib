/*
 *  COPYRIGHT (c) 2019.
 *  Cesar Fuguet-Tortolero
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.rtems.org/license/LICENSE.
 */
/*
 *  COPYRIGHT (c) 1989-1999.
 *  On-Line Applications Research Corporation (OAR).
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.rtems.org/license/LICENSE.
 */

#ifndef LIBBSP_RISCV_RV32_SOCLIB_XCACHE_BSP_H
#define LIBBSP_RISCV_RV32_SOCLIB_XCACHE_BSP_H

/**
 * @addtogroup RTEMSBSPsRiscv
 *
 * @{
 */
#include <rtems.h>
#include <rtems/clockdrv.h>
#include <rtems/console.h>

#include <bspopts.h>
#include <bsp/default-initial-extension.h>

#define CONSOLE_MAX_DEVICES 4

#define bsp_frequency 32000000ULL /* 32 MHz */
#define ticks_to_ms(ticks) (((ticks) * 1000) / (bsp_frequency))
#define ms_to_ticks(ms) (((bsp_frequency) / 1000) * (ms))

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

/** @} */

#endif
