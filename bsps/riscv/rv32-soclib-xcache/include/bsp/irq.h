/*
 *  COPYRIGHT (c) 2019.
 *  Cesar Fuguet-Tortolero
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.rtems.org/license/LICENSE.
 */
#ifndef LIBBSP_RISCV_RV32_SOCLIB_XCACHE_IRQ_H_
#define LIBBSP_RISCV_RV32_SOCLIB_XCACHE_IRQ_H_

#define BSP_INTERRUPT_VECTOR_MIN 0
#define BSP_INTERRUPT_VECTOR_MAX 1
#define INTERRUPT_VECTOR_TIMER 0
#define INTERRUPT_VECTOR_IPI   1

#include <rtems/irq.h>
#include <rtems/irq-extension.h>
#include <rtems/score/processormask.h>

struct Per_CPU_Control;

uintptr_t bsp_get_per_cpu_xicu_address(struct Per_CPU_Control *cpu_self);

void bsp_interrupt_set_affinity(
  rtems_vector_number vector,
  const Processor_mask *affinity);

void bsp_interrupt_get_affinity(
  rtems_vector_number vector,
  Processor_mask *affinity);

#endif
