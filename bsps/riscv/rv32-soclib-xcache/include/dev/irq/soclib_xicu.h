/*
 *  COPYRIGHT (c) 2019
 *  Cesar Fuguet Tortolero
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.rtems.org/license/LICENSE.
 */
#ifndef __SOCLIB_XICU_H__
#define __SOCLIB_XICU_H__

#include <rtems/score/basedefs.h>

#define XICU_NB_PTI 16
#define XICU_NB_HWI 16
#define XICU_NB_WTI 16
#define XICU_NB_OUT 16

enum xicu_regs_e {
  XICU_WTI_REG         = 0,
  XICU_PTI_PER         = 1,
  XICU_PTI_VAL         = 2,
  XICU_PTI_ACK         = 3,
  XICU_MSK_PTI         = 4,
  XICU_MSK_PTI_ENABLE  = 5,
  XICU_MSK_PTI_DISABLE = 6,
  XICU_PTI_ACTIVE      = 6,

  XICU_MSK_HWI         = 8,
  XICU_MSK_HWI_ENABLE  = 9,
  XICU_MSK_HWI_DISABLE = 10,
  XICU_HWI_ACTIVE      = 10,

  XICU_MSK_WTI         = 12,
  XICU_MSK_WTI_ENABLE  = 13,
  XICU_MSK_WTI_DISABLE = 14,
  XICU_WTI_ACTIVE      = 14,

  XICU_PRIO            = 15,

  XICU_CONFIG          = 16,
};

RTEMS_INLINE_ROUTINE
unsigned int __soclib_xicu_regoffset(unsigned int func, unsigned int out)
{
  return (((func & 0x1fu) << 5) | (out & 0x1fu)) << 2;
}


RTEMS_INLINE_ROUTINE
uint32_t __soclib_xicu_get_register(uintptr_t addr)
{
  volatile uint32_t* reg = (volatile uint32_t*)addr;
  return *reg;
}


RTEMS_INLINE_ROUTINE
void __soclib_xicu_set_register(uintptr_t addr, uint32_t val)
{
  volatile uint32_t* reg = (volatile uint32_t*)addr;
  RTEMS_COMPILER_MEMORY_BARRIER();
  *reg = (volatile uint32_t)val;
}


RTEMS_INLINE_ROUTINE
uint32_t soclib_xicu_timer_get_mask(uintptr_t addr, int out)
{
  uintptr_t reg;

  if (out >= XICU_NB_OUT)
    return 0;

  reg = addr + __soclib_xicu_regoffset(XICU_MSK_PTI, out);
  return __soclib_xicu_get_register(reg);
}


RTEMS_INLINE_ROUTINE
void soclib_xicu_timer_disable_all(uintptr_t addr, int out)
{
  uintptr_t reg;

  if (out >= XICU_NB_OUT)
    return;

  reg = addr + __soclib_xicu_regoffset(XICU_MSK_PTI, out);
  __soclib_xicu_set_register(reg, 0);
}


RTEMS_INLINE_ROUTINE
void soclib_xicu_timer_set_period(uintptr_t addr, int timer, uint32_t period)
{
  uintptr_t reg;

  if (timer >= XICU_NB_PTI)
    return;

  reg = addr + __soclib_xicu_regoffset(XICU_PTI_PER, timer);
  __soclib_xicu_set_register(reg, period);
}


RTEMS_INLINE_ROUTINE
uint32_t soclib_xicu_timer_get_val(uintptr_t addr, int timer)
{
  uintptr_t reg;

  if (timer >= XICU_NB_PTI)
    return 0;

  reg = addr + __soclib_xicu_regoffset(XICU_PTI_VAL, timer);
  return __soclib_xicu_get_register(reg);
}


RTEMS_INLINE_ROUTINE
void soclib_xicu_timer_set_val(uintptr_t addr, int timer, uint32_t val)
{
  uintptr_t reg;

  if (timer >= XICU_NB_PTI)
    return;

  reg = addr + __soclib_xicu_regoffset(XICU_PTI_VAL, timer);
  __soclib_xicu_set_register(reg, val);
}


RTEMS_INLINE_ROUTINE
void soclib_xicu_timer_enable(uintptr_t addr, int timer, int out)
{
  uintptr_t reg;

  if (timer >= XICU_NB_PTI)
    return;
  if (out >= XICU_NB_OUT)
    return;

  reg = addr + __soclib_xicu_regoffset(XICU_MSK_PTI_ENABLE, out);
  __soclib_xicu_set_register(reg, 1 << timer);
}


RTEMS_INLINE_ROUTINE
void soclib_xicu_timer_disable(uintptr_t addr, int timer, int out)
{
  uintptr_t reg;

  if (timer >= XICU_NB_PTI)
    return;
  if (out >= XICU_NB_OUT)
    return;

  reg = addr + __soclib_xicu_regoffset(XICU_MSK_PTI_DISABLE, out);
  __soclib_xicu_set_register(reg, 1 << timer);
}


RTEMS_INLINE_ROUTINE
uint32_t soclib_xicu_timer_get_active(uintptr_t addr, int out)
{
  uintptr_t reg;

  if (out >= XICU_NB_OUT)
    return 0;

  reg = addr + __soclib_xicu_regoffset(XICU_PTI_ACTIVE, out);
  return __soclib_xicu_get_register(reg);
}


RTEMS_INLINE_ROUTINE
void soclib_xicu_timer_ack(uintptr_t addr, int timer)
{
  uintptr_t reg;

  if (timer >= XICU_NB_PTI)
    return;

  reg = addr + __soclib_xicu_regoffset(XICU_PTI_ACK, 1 << timer);
  __soclib_xicu_get_register(reg);
}


RTEMS_INLINE_ROUTINE
uint32_t soclib_xicu_wti_get_mask(uintptr_t addr, int out)
{
  uintptr_t reg;

  if (out >= XICU_NB_OUT)
    return 0;

  reg = addr + __soclib_xicu_regoffset(XICU_MSK_WTI, out);
  return __soclib_xicu_get_register(reg);
}


RTEMS_INLINE_ROUTINE
void soclib_xicu_wti_enable(uintptr_t addr, int wti, int out)
{
  uintptr_t reg;

  if (wti >= XICU_NB_WTI)
    return;
  if (out >= XICU_NB_OUT)
    return;

  reg = addr + __soclib_xicu_regoffset(XICU_MSK_WTI_ENABLE, out);
  __soclib_xicu_set_register(reg, 1 << wti);
}


RTEMS_INLINE_ROUTINE
void soclib_xicu_wti_disable(uintptr_t addr, int wti, int out)
{
  uintptr_t reg;

  if (wti >= XICU_NB_WTI)
    return;
  if (out >= XICU_NB_OUT)
    return;

  reg = addr + __soclib_xicu_regoffset(XICU_MSK_WTI_DISABLE, out);
  __soclib_xicu_set_register(reg, 1 << wti);
}


RTEMS_INLINE_ROUTINE
void soclib_xicu_wti_send(uintptr_t addr, int wti, uint32_t val)
{
  uintptr_t reg;

  if (wti >= XICU_NB_WTI)
    return;

  reg = addr + __soclib_xicu_regoffset(XICU_WTI_REG, wti);
  __soclib_xicu_set_register(reg, val);
}


RTEMS_INLINE_ROUTINE
uint32_t soclib_xicu_wti_recv(uintptr_t addr, int wti)
{
  uintptr_t reg;

  if (wti >= XICU_NB_WTI)
    return 0;

  reg = addr + __soclib_xicu_regoffset(XICU_WTI_REG, wti);
  return __soclib_xicu_get_register(reg);
}


RTEMS_INLINE_ROUTINE
uint32_t soclib_xicu_wti_get_active(uintptr_t addr, int out)
{
  uintptr_t reg;

  if (out >= XICU_NB_OUT)
    return 0;

  reg = addr + __soclib_xicu_regoffset(XICU_WTI_ACTIVE, out);
  return __soclib_xicu_get_register(reg);
}


RTEMS_INLINE_ROUTINE
uint32_t soclib_xicu_pti_count(uintptr_t addr)
{
  uintptr_t reg;

  reg = addr + __soclib_xicu_regoffset(XICU_CONFIG, 0);
  return __soclib_xicu_get_register(reg) & 0x3f;
}


RTEMS_INLINE_ROUTINE
uint32_t soclib_xicu_wti_count(uintptr_t addr)
{
  uintptr_t reg;

  reg = addr + __soclib_xicu_regoffset(XICU_CONFIG, 0);
  return (__soclib_xicu_get_register(reg) >> 16) & 0x3f;
}


RTEMS_INLINE_ROUTINE
uint32_t soclib_xicu_hwi_count(uintptr_t addr)
{
  uintptr_t reg;

  reg = addr + __soclib_xicu_regoffset(XICU_CONFIG, 0);
  return (__soclib_xicu_get_register(reg) >> 8) & 0x3f;
}


RTEMS_INLINE_ROUTINE
uint32_t soclib_xicu_irq_count(uintptr_t addr)
{
  uintptr_t reg;

  reg = addr + __soclib_xicu_regoffset(XICU_CONFIG, 0);
  return (__soclib_xicu_get_register(reg) >> 24) & 0x3f;
}

#endif /* __SOCLIB_XICU_H__ */
