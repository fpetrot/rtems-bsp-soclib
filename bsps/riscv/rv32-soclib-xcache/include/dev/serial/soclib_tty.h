/*
 *  COPYRIGHT (c) 2019
 *  Cesar Fuguet Tortolero
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.rtems.org/license/LICENSE.
 */
#ifndef __SOCLIB_TTY_H__
#define __SOCLIB_TTY_H__

#include <stdint.h>

/*
 *
 */
void soclib_tty_putc(
  uintptr_t base,
  int channel,
  char c
);

/*
 *
 */
int soclib_tty_getc(
  uintptr_t base,
  int channel
);

/*
 *
 */
int soclib_tty_has_char(
  uintptr_t base,
  int channel
);


#endif /* __SOCLIB_TTY_H__ */
