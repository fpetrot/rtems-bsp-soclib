/*
 *  COPYRIGHT (c) 2019.
 *  Cesar Fuguet-Tortolero
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.rtems.org/license/LICENSE.
 */
/*
 *  COPYRIGHT (c) 1989-2014.
 *  On-Line Applications Research Corporation (OAR).
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.rtems.org/license/LICENSE.
 */
#include <rtems.h>
#include <rtems/timecounter.h>
#include <rtems/score/percpu.h>
#include <rtems/score/riscv-utility.h>
#include <rtems/irq-extension.h>
#include <bsp.h>
#include <bsp/irq.h>
#include <bsp/fatal.h>
#include <stdlib.h>

#include "dev/irq/soclib_xicu.h"

typedef struct {
  struct timecounter tc;
  uint32_t  ticks_interval;
  uintptr_t base;   /* XICU base address */
  uint32_t  index;  /* XICU timer index */
  uint32_t  out;    /* XICU output index */
} timecounter_context;

static timecounter_context __sys_tc;

static uint32_t __clock_timecount_get(struct timecounter *tc)
{
  return read_csr(mcycle);
}


static void __clock_at_tick(timecounter_context *clk)
{
  /* acknowledge the timer interrupt and reinitialize the counter */
  soclib_xicu_timer_set_period(clk->base, clk->index, 0);

  /* re-enable the timer */
  soclib_xicu_timer_set_period(clk->base, clk->index, clk->ticks_interval);
}


static void __clock_install_isr(rtems_interrupt_handler isr)
{
  rtems_status_code sc = rtems_interrupt_handler_install(
      INTERRUPT_VECTOR_TIMER,
      "Clock",
      RTEMS_INTERRUPT_UNIQUE,
      isr,
      NULL);

  if (sc != RTEMS_SUCCESSFUL)
    bsp_fatal(RISCV_FATAL_CLOCK_IRQ_INSTALL);
}


static void __clock_install(void)
{
  uint32_t us_per_tick;

  /* compute the tick interval */
  us_per_tick = rtems_configuration_get_microseconds_per_tick();
  __sys_tc.ticks_interval = ms_to_ticks(us_per_tick / 1000);

  /* initialize timecounter driver */
  __sys_tc.tc.tc_get_timecount = __clock_timecount_get;
  __sys_tc.tc.tc_counter_mask = 0xfffffffful;
  __sys_tc.tc.tc_frequency = bsp_frequency;
  __sys_tc.tc.tc_quality = RTEMS_TIMECOUNTER_QUALITY_CLOCK_DRIVER;
  rtems_timecounter_install(&__sys_tc.tc);

  /* enable the tick timer */
  soclib_xicu_timer_enable(__sys_tc.base, __sys_tc.index, __sys_tc.out);

  /* initialize the tick timer (as does at every tick from now on) */
  __clock_at_tick(&__sys_tc);
}


static void __clock_initialize(void)
{
  Per_CPU_Control *cpu = _Per_CPU_Get_by_index(0);

  __sys_tc.base = (uintptr_t)cpu->cpu_per_cpu.plic_hart_regs;
  __sys_tc.index = 0;
  __sys_tc.out = 0;

  __clock_install();
}

#define Clock_driver_support_at_tick() \
  __clock_at_tick(&__sys_tc)

#define Clock_driver_support_install_isr(isr) \
  __clock_install_isr(isr)

#define Clock_driver_support_initialize_hardware() \
  __clock_initialize()

#define CLOCK_DRIVER_USE_ONLY_BOOT_PROCESSOR

#include <../../../bsps/shared/dev/clock/clockimpl.h>
