# Instructions to build the Board Support Package (BSP) with the RTEMS kernel for the SoCLib's platforms.

[[_TOC_]]

##  Expected environment

WORKDIR is the base directory where all sources are available. In this base
directories, you can find too the directories with the generated object files
after installation.

The WORKDIR tree is the following:
```shell
${WORKDIR}/rtems-sources/rtems             # RTEMS kernel sources
${WORKDIR}/rtems-sources/rsb               # RTEMS utility scripts
${WORKDIR}/rtems-sources/docs              # RTEMS documentation
${WORKDIR}/rtems-sources/rtems-bsp-soclib  # RTEMS BSP sources for Soclib
${WORKDIR}/rtems-binaries/5                # RTEMS RISC-V crosstool
```

##  Instructions for building the CROSS-TOOLCHAIN
```shell
ARCH=riscv
RTEMS_RELEASE=5

# The following dependencies need to be installed (for a Debian host)
apt-get install \
    build-dep build-essential gcc-defaults g++ gdb git unzip pax bison flex \
    texinfo unzip python3-dev libpython-dev libncurses5-dev zlib1g-dev

# Check that the host packages needed are present
cd ${WORKDIR}/rtems-sources/rsb/rtems
../source-builder/sb-check

# Build the cross-toolchain for the target architecture
mkdir -p ${WORKDIR}/rtems-binaries/${RTEMS_RELEASE}
../source-builder/sb-set-builder \
    --prefix=${WORKDIR}/rtems-binaries/${RTEMS_RELEASE} \
    ${RTEMS_RELEASE}/rtems-${ARCH}
```

##  Instructions for building the BSP
```shell
BSP=rv32-soclib-xcache

# copy the target BSP into the RTEMS kernel source tree
cp -r ${WORKDIR}/rtems-sources/rtems-bsp-soclib/bsps ${WORKDIR}/rtems-sources/rtems
cp -r ${WORKDIR}/rtems-sources/rtems-bsp-soclib/c ${WORKDIR}/rtems-sources/c

# bootstrap the compiling configuration of the RTEMS kernel
cd ${WORKDIR}/rtems-sources/rtems

# clean and generate new configuration scripts
./bootstrap -c
./bootstrap -p
${WORKDIR}/rtems-sources/rsb/sb-bootstrap

# create directory for building the RTEMS kernel
mkdir -p ${WORKDIR}/rtems-build/build-${BSP}
cd ${WORKDIR}/rtems-build/build-${BSP}

# set path to the RISCV toolchain (compiled for RTEMS)
RTEMS_PREFIX=${WORKDIR}/rtems-binaries/${RTEMS_RELEASE}
export PATH=${PATH}:${RTEMS_PREFIX}/bin

# configure
${WORKDIR}/rtems-sources/rtems/configure \
    --prefix=${RTEMS_PREFIX} \
    --enable-maintainer-mode \
    --target=${ARCH}-rtems${RTEMS_RELEASE} \
    --enable-rtemsbsp=${BSP} \
    --enable-tests \
    --enable-smp \
    --enable-posix \
    --enable-cxx

# build
make -j<N> |& tee build.log

# install
make -j<N> install |& install.log
```
